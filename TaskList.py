import sqlhelper
import uuid
import datetime


def make_task():
    Id = str(uuid.uuid4())
    Name = input('prosze podac nazwe')
    CreateDay = str(datetime.datetime.now())
    DeadLine = int(input('data ukonczenia projektu'))
    Status = 'Incomplete'
    sqlhelper.create_task(Id, Name, CreateDay, DeadLine, Status)

    menu()


def change_status():
    Id = input('please enter a project Id:')
    task_list = sqlhelper.find_project_by_id(Id)
    if not len(task_list) > 0:
        print('There is no such project')
        menu()
    Status = 'Complete'
    sqlhelper.make_complete_status(Status, Id)
    menu()


def remove_task():
    Id = input('please enter a project Id')
    Status = 'Complete'
    task_list = sqlhelper.find_project_by_id(Id)
    if not len(task_list) > 0:
        print('Incorrect datas')
        menu()

    sqlhelper.delete_complete_task(Id, Status)
    print('task removed')
    menu()


def show_task():
    print(sqlhelper.show_all_tasks())
    menu()


def menu():
    print('''
============MENU===============    
1. Creating a task
2. Removing task
3. Editing the task
4. Show all tasks
''')
    choice = int(input('please choose optione'))

    if (choice == 1):
        make_task()

    elif (choice == 2):
        remove_task()

    elif (choice == 3):
        change_status()

    elif (choice == 4):
        show_task()

menu()
