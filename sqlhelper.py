import mysql.connector as mysql
import datetime


def create_task(Id, Name, CreateDay, DeadLine, Status):
    mydb = mysql.connect(
        host='localhost',
        user='root',
        passwd='root',
        database='baza'
    )
    mycursor = mydb.cursor()
    sql = "INSERT INTO tasklist(Id, Name, CreateDay, DeadLine, Status) VALUES (%s, %s, %s, %s, %s)"
    values = (Id, Name, CreateDay, DeadLine, Status)
    mycursor.execute(sql, values)
    mydb.commit()


def make_complete_status(Status, Id):
    mydb = mysql.connect(
        host='localhost',
        user='root',
        passwd='root',
        database='baza'
    )
    mycursor = mydb.cursor()
    sql = f"UPDATE tasklist SET Status = '{Status}'WHERE Id like '{Id}'"
    mycursor.execute(sql)
    mydb.commit()


def find_project_by_id(Id):
    mydb = mysql.connect(
        host='localhost',
        user='root',
        passwd='root',
        database='baza'
    )
    mycursor = mydb.cursor()
    sql = f"SELECT * FROM tasklist WHERE Id LIKE '{Id}'"
    mycursor.execute(sql)
    return mycursor.fetchall()


def delete_complete_task(Id, Status):
    mydb = mysql.connect(
        host='localhost',
        user='root',
        passwd='root',
        database='baza'
    )
    mycursor = mydb.cursor()
    sql = f"DELETE FROM tasklist WHERE Id LIKE '{Id}'  AND Status LIKE '{Status}'"
    mycursor.execute(sql)
    mydb.commit()

def show_all_tasks():
    mydb = mysql.connect(
        host='localhost',
        user='root',
        passwd='root',
        database='baza'
    )
    mycursor = mydb.cursor()
    sql = "SELECT Id, Name FROM tasklist"
    mycursor.execute(sql)
    return mycursor.fetchall()